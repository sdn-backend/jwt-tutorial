## JWT TUTORIAL

### 주요 코드 설명 🖐🏻
![img.png](img.png)

`User.class`
```java
@ManyToMany
@JoinTable(
    name = "user_authority",
    joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")},
    inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "authority_name")})
private Set<Authority> authorities;
```
- ⭐ 다대다 관계를 1대다 다대1 관계로 조인 테이블로 정의했다는 의미

`data.sql`
```roomsql
INSERT INTO USER (USER_ID, USERNAME, PASSWORD, NICKNAME, ACTIVATED) VALUES (1, 'admin', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi', 'admin', 1);
INSERT INTO USER (USER_ID, USERNAME, PASSWORD, NICKNAME, ACTIVATED) VALUES (2, 'user', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', 'user', 1);

INSERT INTO AUTHORITY (AUTHORITY_NAME) values ('ROLE_USER');
INSERT INTO AUTHORITY (AUTHORITY_NAME) values ('ROLE_ADMIN');

INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_NAME) values (1, 'ROLE_USER');
INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_NAME) values (1, 'ROLE_ADMIN');
INSERT INTO USER_AUTHORITY (USER_ID, AUTHORITY_NAME) values (2, 'ROLE_USER');
```
- 서버 실행될때마다 자동으로 데이터 생성 및 실행하는 코드 

#### ✔ h2 콘솔 접근방법
- `http://localhost:9090/h2-console/` 

#### ✔ CustomUserDetailsService.class
- UserDetailsService 구현한 클래스
```java
@Component("userDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String username) { ⭐ 로그인시에 db에서 유저정보, 권한정보 가져온다. 
        return userRepository.findOneWithAuthoritiesByUsername(username) / 해당 정보를 기반으로 userdetails.User 객체 생성
                .map(user -> createUser(username, user))
                .orElseThrow(() -> new UsernameNotFoundException(username + " -> 데이터베이스에서 찾을 수 없습니다."));
    }

    private org.springframework.security.core.userdetails.User createUser(String username, User user) {
        if (!user.isActivated()) {
            throw new RuntimeException(username + " -> 활성화되어 있지 않습니다.");
        }
        List<GrantedAuthority> grantedAuthorities = user.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthorityName()))
                .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                grantedAuthorities);
    }
}
```

## JWT 로직 개발 ⭐
### 1. JWT 설정 추가
```yml
jwt:
  header: Authorization
  #HS512 알고리즘을 사용할 것이기 때문에 512bit, 즉 64byte 이상의 secret key를 사용해야 한다.
  #echo 'silvernine-tech-spring-boot-jwt-tutorial-secret-silvernine-tech-spring-boot-jwt-tutorial-secret'|base64
  secret: c2lsdmVybmluZS10ZWNoLXNwcmluZy1ib290LWp3dC10dXRvcmlhbC1zZWNyZXQtc2lsdmVybmluZS10ZWNoLXNwcmluZy1ib290LWp3dC10dXRvcmlhbC1zZWNyZXQK
  token-validity-in-seconds: 86400
```
`jwt 라이브러리 설치`
```roomsql
implementation group: 'io.jsonwebtoken', name: 'jjwt-api', version: '0.11.2'
runtimeOnly group: 'io.jsonwebtoken', name: 'jjwt-impl', version: '0.11.2'
runtimeOnly group: 'io.jsonwebtoken', name: 'jjwt-jackson', version: '0.11.2'
```
### 2. JWT 관련 코드 개발
- `Token Provider`: 토큰의 생성, 토큰의 유효성 검증등을 담당
- `JwtFilter`: JWT를 위한 커스텀 필터를 만드는 담당
    - `doFilter`: jwt 토큰의 인증정보를 현재 실행 중인 SecurityContext에 저장하는 역할 수행
    - `resolveToken method`: Request Header에서 토큰 정보를 꺼내오기 위한 매서드
- `JwtSecurityConfig`: Token Provider, JwtFiler를 SecurityConfig에 적용할 때 사용하는 클래스
- `JwtAuthenticationEntryPoint`: 유효한 자격증명을 제공하지 않고 접근하려 할때 401 Unauthorized 에러를 리턴하는 클래스
- `JwtAccessDeinedHandler`: 필요한 권한이 존재하지 않는 경우에 403 Forbidden 에러 리턴하기 위한 클래스

### 3. Security 설정 추가
- 위의 5가지 클래스를 시큐리티 설정하기

## 컨트롤러
### `AuthController`
![img_1.png](img_1.png)

### `UserController`
![img_2.png](img_2.png)